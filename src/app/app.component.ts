import { Component } from '@angular/core';
import { ScrollService } from '../app/services/scroll.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [ScrollService] 
})

export class AppComponent {

  constructor(public _scrollService: ScrollService) {}
  
  ngOnInit() {
    this.moveTop();
  }

  moveTop(){
    setTimeout(() => {
      this._scrollService.scrollToTop();
   }, 1000);
  }
}
