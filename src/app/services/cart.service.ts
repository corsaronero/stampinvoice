
import { Injectable, Inject } from '@angular/core';
import { IProduct, IDetail } from '../models/products';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { APP_CONFIG, IAppConfig } from '../app_config/app.config';


@Injectable()
export class CartService{

    public itemsInCartSubject: BehaviorSubject<IDetail[]> = new BehaviorSubject([]);
    itemsInCartObs = this.itemsInCartSubject.asObservable();

    private itemsInCart: IDetail[] = [];
    currency_code:any;

    public itemsNetSubject: BehaviorSubject<number[]> = new BehaviorSubject([]);
    itemsNetObs = this.itemsNetSubject.asObservable();

    private itemsNet: any = [];

    cartLengthSubject = new BehaviorSubject<any>([]);
    itemsInCartLength = this.cartLengthSubject.asObservable();

    constructor(@Inject(APP_CONFIG) appConfig: IAppConfig){
        this.currency_code = appConfig.currency_code;
    }

    addToCart():void{
            this.itemsInCart.push({
                code: '',
                description: '',
                quantity: 1,
                unitPriceWithVat: 0.00,
                vatRate: 22,
                
            });
            this.itemsNet.push({
                net: 0.00
            });
            console.log("net", this.itemsNet)
            this.itemsInCartSubject.next(this.itemsInCart);
            this.itemsNetSubject.next(this.itemsNet);
        }

    addQtyItems(index:number, qty):any{
        this.itemsInCart[index].quantity = this.itemsInCart[index].quantity + qty;

        this.cartLengthSubject.next(this.itemsInCart.length);
        this.itemsInCartSubject.next(this.itemsInCart);
    }

    removeQtyItems(index:number,qty:number):any{
        this.itemsInCart[index].quantity = this.itemsInCart[index].quantity - qty;

        this.cartLengthSubject.next(this.itemsInCart.length);
        this.itemsInCartSubject.next(this.itemsInCart);
    }

    changePriceInCart(index:number, price:number){
        this.itemsInCart[index].unitPriceWithVat = price;
        this.itemsInCartSubject.next(this.itemsInCart);
    }

    changeVatInCart(index:number, vat:number){
        this.itemsInCart[index].vatRate = vat;
        this.itemsInCartSubject.next(this.itemsInCart);
    }

    changeNetInCart(index:number, result:number){
        this.itemsNet[index].net = result;
        this.itemsNetSubject.next(this.itemsNet);
        this.itemsInCartSubject.next(this.itemsInCart);
    }

    //Remove group of product from cart
    removeItem(index):void{
        this.itemsInCart.splice(index, 1);
        this.itemsNet.splice(index, 1);
        this.itemsInCartSubject.next(this.itemsInCart);
        this.itemsNetSubject.next(this.itemsNet);
    }

    //Total quantity of product in cart
    public getTotalQty(): Observable<number> {
        return this.itemsInCartSubject.pipe(map((items: any[]) => {
          return items.reduce((prev, curr: any) => {
              return prev + (curr.quantity);
          }, 0);
        }));
      }

    //SubTotal amount in cart product : Observable<number>
    public getSubTotalAmount() {
        let subtotal: number = 0;
        for(var i = 0; i < this.itemsNet.length; i++){
            subtotal = subtotal + (this.itemsNet[i].net);
        }
        return subtotal;
    }

    //Total amount in cart product
    public getTotalAmount() {
        let total: number = 0;
        for(var i = 0; i < this.itemsInCart.length; i++){
            total = total + (this.itemsInCart[i].unitPriceWithVat * this.itemsInCart[i].quantity);
        }
        return total;
    }

}
