import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders,HttpParams } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { APP_CONFIG, IAppConfig } from '../app_config/app.config';

@Injectable()
export class DataService {

    private _dataUrl: string;
    private headers = new HttpHeaders({
        'Content-Type': 'application/json'
    });
    
    invoiceNumberSource = new BehaviorSubject<string>('');
    invoiceNumberObs = this.invoiceNumberSource.asObservable();

    constructor(@Inject(APP_CONFIG) appConfig: IAppConfig, private _http: HttpClient) {
        this._dataUrl = appConfig.api.data;
    }

    // prod API -> list of products : Observable < any >
    postDataAPI(items) {
        return this._http
            .post(this._dataUrl, items, { headers: this.headers }).subscribe(
              res => { 
                this.invoiceNumberSource.next(res as any);
              },
              error => console.log('Error Products Service - HTTP GET Service', error, error.message)
            );
        }
}