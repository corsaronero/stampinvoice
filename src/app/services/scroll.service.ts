
import { Injectable } from '@angular/core';
import * as smoothscroll from 'smoothscroll-polyfill';


@Injectable()
export class ScrollService {
    private isBrowser: boolean;

    constructor() {}

    public scrollToTop() {
          window.scroll({ top: 0, left: 0, behavior: 'smooth' });
      }
}


