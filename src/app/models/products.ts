
export interface IProduct {
    discount:number;
    items:IDetail[];
}

export interface IDetail{
    code: string;
    description: string;
    quantity: number;
    unitPriceWithVat:number;
    vatRate:number;
}
