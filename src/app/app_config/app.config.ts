import { InjectionToken } from '@angular/core';

export interface IAppConfig {
    api: any;
    currency_code:string;
}

export let APP_CONFIG = new InjectionToken('app.config');

export const AppConfig: IAppConfig = {

    api: {
      data: `https://stamp-interview-apis.azurewebsites.net/api/invoices`,
    },
    currency_code:'EUR'
  };