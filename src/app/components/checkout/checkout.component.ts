import { Component, OnInit, Inject } from '@angular/core';
import { DataService } from '../../services/data.service';
import { CartService } from '../../services/cart.service';
import { IProduct, IDetail } from '../../models/products';
import { ScrollService } from '../../services/scroll.service';
import { FormGroup,  FormBuilder, FormArray,  Validators, AbstractControl } from '@angular/forms';
import { Observable, Subscription, BehaviorSubject } from 'rxjs';
import { APP_CONFIG, IAppConfig } from '../../app_config/app.config';

export interface IProduct2 {
  code: string;
  description: string;
  quantity: number;
  unitPriceWithVat:number;
  vatRate:number;
  net:number;
}

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss'],
  providers:[ScrollService]
})
export class CheckoutComponent implements OnInit {

  cartItems:IDetail[];
  netItems:number[];
  cartLength: any;
  errorMessage: string;
  currency_code:any;
  totalAmount:any;
  totalPrice:number = 0;
  subTotalPrice:number = 0;
  percentageVat:number = 22;
  discount:number = 0.00;
  formSubmitAttempt: boolean;
  productForm: FormGroup;
  invoiceNumber:string;
  disableInput: boolean = false;


  private subscription: Subscription;

  public itemsTotalPriceSubject: BehaviorSubject<number> = new BehaviorSubject(0);
  itemsTotalPriceObs = this.itemsTotalPriceSubject.asObservable();

  public itemsSubTotalPriceSubject: BehaviorSubject<number> = new BehaviorSubject(0);
  itemsSubTotalPriceObs = this.itemsSubTotalPriceSubject.asObservable();

  private invoiceReq: IProduct[] = [];
  
  constructor(
    private _dataService: DataService, 
    private _formBuilder: FormBuilder, 
    @Inject(APP_CONFIG) appConfig: IAppConfig,
    private _cartService:CartService,
    public _scrollService: ScrollService
    ) { 
    this.currency_code = appConfig.currency_code;
  }

  ngOnInit() {

    this.addItemInCart();

    this.subscription = this.itemsTotalPriceObs.subscribe(res => {
      this.totalPrice = res;
    })

    this.subscription = this.itemsSubTotalPriceObs.subscribe(res => {
      this.subTotalPrice = res;
    })
    
    this.subscription = this._cartService.itemsInCartObs.subscribe(res => {
      this.cartItems = res;
    });

    this.subscription = this._cartService.itemsNetObs.subscribe(res => {
      this.netItems = res;
    });

    this.subscription = this._dataService.invoiceNumberObs.subscribe(res => {
      this.invoiceNumber = (res as any).invoiceNumber;
      console.log("this.invoiceNumber", this.invoiceNumber)

      if(this.invoiceNumber != undefined){
        this.productForm.disable();
        this.disableInput = true;
      }
    })

    this.invoiceReq.push({
      discount:0,
      items:this.cartItems
    });
   
    this.productForm = this._formBuilder.group({
      selling_points: this._formBuilder.array([this._formBuilder.group(
        {code:'', 
        description: ['', [this.noWhitespaceValidator]],
        quantity:1, 
        price: ['', [Validators.required]],
        vatRate:22, 
        net:''}
      )])
    });

  }

  get sellingPoints() {
    return this.productForm.get('selling_points') as FormArray;
  }

  addSellingPoint() {
    this.sellingPoints.push(this._formBuilder.group(
      {code:'', 
      description:['', [this.noWhitespaceValidator]], 
      quantity:1, 
      price: ['', [Validators.required]],
      vatRate:['22%'], 
      net:''}
      ));

    this.addItemInCart();
    console.log(this.sellingPoints.controls)
  }

  deleteSellingPoint(index) {
    this.sellingPoints.removeAt(index);
    this.removeItemFromCart(index);
  }

  //Control least 5 characters long
  public noWhitespaceValidator(control: AbstractControl): { [key: string]: boolean } | null  {
    let isWhitespace;
    if(control.value != null)
      isWhitespace = control.value.replace(/\s/g, "").toLowerCase();
      if(isWhitespace.length < 5){
        return { 'validLength': true };
      }
    return null;
  }

  //Add single Row
  addItemInCart(){
    this._cartService.addToCart();
    this.percentageVat = 22; 
  }

  //Remove single Row
  removeItemFromCart(index): void {
    this._cartService.removeItem(index);
    this.invoiceReq[0].discount = this.discount = 0.00;
    this.getSubTotal();
    this.getTotal();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  //Change price calculate Net
  onKey(index:number, price: any, qty:number) { 
    
    let priceValue = parseFloat(price.replace(/[^0-9.]+/g, ''));
    this.percentageVat = this.cartItems[index].vatRate;
    this.calculationNet(index, this.percentageVat, priceValue, qty);

    this._cartService.changePriceInCart(index, priceValue);
    this._cartService.changeVatInCart(index, this.percentageVat);
    this.getTotal();
  }

  //Select VAT calculate Net
  onChange(index:number, price:any, value, qty:number){
    this.percentageVat = parseInt(value);
    this._cartService.changeVatInCart(index, this.percentageVat);

    if( price != 0)
     this.calculationNet(index, this.percentageVat, price, qty);
  }

  //Total quantity of product in cart
  public getTotalQty(): Observable<number> {
    return this._cartService.getTotalQty();
  }

  //Add quantity or product
  addQtyItemInCart(index:number, qty:number, price:any){
    this._cartService.addQtyItems(index, qty);
    this.percentageVat = this.cartItems[index].vatRate;
    let quantity = this.cartItems[index].quantity;

    if( price != 0)
      this.calculationNet(index, this.percentageVat, price, quantity);
  }

  //Remove quantity of product
  removeQtyItemToCart(index:number, qty:number, price:any){
    this._cartService.removeQtyItems(index, qty);
    this.percentageVat = this.cartItems[index].vatRate;
    let quantity = this.cartItems[index].quantity;

    if( price != 0)
      this.calculationNet(index, this.percentageVat, price, quantity);
  }

  //Net Calculation
  calculationNet(index, percentageVat, price, qty){
    let div = 100 + percentageVat;
    let result = ( 100 * (price * qty) ) / div;
    this._cartService.changeNetInCart(index, result);
    this.getSubTotal();
    this.getTotal();
  }

  getSubTotal(){
    this.subTotalPrice = this._cartService.getSubTotalAmount() - this.invoiceReq[0].discount;
    this.itemsSubTotalPriceSubject.next(this.subTotalPrice);
    return this.subTotalPrice;
  }

  //Total amount
  getTotal(){
      this.totalPrice = this._cartService.getTotalAmount();
      this.itemsTotalPriceSubject.next(this.totalPrice);
      return this.totalPrice;
  }

  addCode(index:number, code:string){
    this.cartItems[index].code = code;
  }

  addDescription(index:number, description:string){
    this.cartItems[index].description = description;
  }

  //Add discount at the Total
  addDiscount(discount:any){
    this.discount = parseFloat(discount.replace(/[^0-9.]+/g, ''));
    this.subTotalPrice = this._cartService.getSubTotalAmount() - this.discount;

    this.invoiceReq[0].discount = this.discount;
  }

  getRound(){
    this.totalPrice = Math.floor(this._cartService.getTotalAmount());
    this.itemsTotalPriceSubject.next(this.totalPrice);
  }

  onSubmit() {
    this.formSubmitAttempt = true;
    if (this.productForm.valid) {
      this._dataService.postDataAPI(this.invoiceReq[0]);

      setTimeout(() => {
        this._scrollService.scrollToTop();
     }, 1000);
    }
  }

}