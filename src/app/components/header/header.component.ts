import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers:[]
})
export class HeaderComponent implements OnInit {

  invoiceNumber:string;
  issuedOn:Date;
  private subscription: Subscription;

  constructor(private _dataService: DataService) { }

  ngOnInit() {

    this.subscription = this._dataService.invoiceNumberObs.subscribe(res => {
      console.log("UP",res);
      this.invoiceNumber = (res as any).invoiceNumber;
      this.issuedOn = (res as any).issuedOn;
    })

  }

}
